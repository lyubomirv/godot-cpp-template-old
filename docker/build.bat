@echo off

where mycommand >nul 2>nul
if %ERRORLEVEL% neq 0 echo Please install Docker & exit /b 1

if [%~1]==[] goto combined
if not [%1]==[--separate] goto combined

:separate
    docker build -t android -f Dockerfile.android .
    docker build -t linux_i386 -f Dockerfile.linux_i386 .
    docker build -t linux_amd64 -f Dockerfile.linux_amd64 .
    docker build -t web -f Dockerfile.web .
    docker build -t windows -f Dockerfile.windows .
    goto done

:combined
    docker build -t combined -f Dockerfile.combined .
    docker build -t web -f Dockerfile.web .
    goto done

:done
    echo Done
