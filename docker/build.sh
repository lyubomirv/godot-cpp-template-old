#!/bin/bash

if ! command -v docker &> /dev/null; then
	echo "Please install Docker"
	exit 1
fi

if [ $# -ge 1 ] && [ "$1" = "--separate" ]; then
	docker build -t android -f Dockerfile.android .
	docker build -t linux_i386 -f Dockerfile.linux_i386 .
	docker build -t linux_amd64 -f Dockerfile.linux_amd64 .
	docker build -t web -f Dockerfile.web .
	docker build -t windows -f Dockerfile.windows .
else
	docker build -t combined -f Dockerfile.combined .
	docker build -t web -f Dockerfile.web .
	docker build -t web -f Dockerfile.windows .
fi
