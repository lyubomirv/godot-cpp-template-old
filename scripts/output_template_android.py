#!/usr/bin/env python3

import common


common.copy_godot_product('android_debug.apk', common.get_templates_output_path())
common.copy_godot_product('android_release.apk', common.get_templates_output_path())
common.copy_godot_product('android_source.zip', common.get_templates_output_path())

