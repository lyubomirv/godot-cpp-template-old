#!/usr/bin/env python3

import common


filename = 'godot.windows.opt.tools.64.exe'
common.exec_strip_command(filename, common.Config.DOCKER_IMAGE_WINDOWS_)
common.copy_godot_product(filename, common.get_editor_output_path())

