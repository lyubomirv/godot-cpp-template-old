#!/usr/bin/env python3

import common


common.exec_strip_command('godot.osx.opt.tools.x86_64')
common.exec_strip_command('godot.osx.opt.tools.arm64')
common.exec_lipo_command(['godot.osx.opt.tools.x86_64', 'godot.osx.opt.tools.arm64'], 'godot.osx.opt.tools.universal')

common.create_macos_app(
	'misc/dist/osx_tools.app',
	['godot.osx.opt.tools.universal'],
	'godot.osx.app', 'godot.osx.zip',
	common.get_editor_output_path())
