#!/usr/bin/env python3

import common


common.strip_and_copy_godot_products(
	['godot.x11.opt.64', 'godot.x11.debug.64'],
	common.get_templates_output_path(),
	common.Config.DOCKER_IMAGE_LINUX_64,
	['linux_x11_64_release', 'linux_x11_64_debug'])

