#!/usr/bin/env python3

import common


common.copy_godot_product('godot.javascript.opt.debug.zip', common.get_templates_output_path(), 'webassembly_debug.zip')
common.copy_godot_product('godot.javascript.opt.zip', common.get_templates_output_path(), 'webassembly_release.zip')

