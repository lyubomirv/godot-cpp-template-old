#!/usr/bin/env python3

import common


filename = 'godot.x11.tools.64'
common.exec_strip_command(filename, common.Config.DOCKER_IMAGE_LINUX_64)
common.copy_godot_product(filename, common.get_editor_output_path())

