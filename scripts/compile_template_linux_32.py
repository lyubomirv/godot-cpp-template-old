#!/usr/bin/env python3

import common

build_command = common.BuildCommand().template().linux().bits_32()

build_command_debug = build_command.debug()
common.exec_build_command(build_command_debug, common.Config.DOCKER_IMAGE_LINUX_32)

build_command_release = build_command.release().no_debug_symbols()
common.exec_build_command(build_command_release, common.Config.DOCKER_IMAGE_LINUX_32)

