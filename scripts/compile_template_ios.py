#!/usr/bin/env python3

import common


def get_config_command(is_release):
	command = common.BuildCommand().ios().no_debug_symbols()
	return command.release() if is_release else command.debug()


common.exec_build_command(get_config_command(False).ios_arm())
common.exec_build_command(get_config_command(False).ios_arm64())
common.exec_build_command(get_config_command(False).ios_x86_64())
common.exec_build_command(get_config_command(True).ios_arm())
common.exec_build_command(get_config_command(True).ios_arm64())
common.exec_build_command(get_config_command(True).ios_x86_64())
