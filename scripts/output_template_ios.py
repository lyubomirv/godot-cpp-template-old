#!/usr/bin/env python3

import os
import shutil
import tempfile
import common


def strip_impl(filename, is_release):
	mode = 'opt' if is_release else 'debug'
	common.exec_strip_command(filename + '.iphone.' + mode + '.arm.a')
	common.exec_strip_command(filename + '.iphone.' + mode + '.arm64.a')
	common.exec_strip_command(filename + '.iphone.' + mode + '.x86_64.a')


def strip(is_release):
	strip_impl('libgodot', is_release)


def strip_all():
	strip(False)
	strip(True)


def lipo_impl(filename, is_release):
	config_str_src = 'opt' if is_release else 'debug'
	config_str_dest = 'release' if is_release else 'debug'
	common.exec_lipo_command(
		[
			filename + '.iphone.' + config_str_src + '.arm.a',
			filename + '.iphone.' + config_str_src + '.arm64.a',
			filename + '.iphone.' + config_str_src + '.x86_64.a'
		],
		filename + '.iphone.' + config_str_dest + '.fat.a')


def lipo(is_release):
	lipo_impl('libgodot', is_release)


def lipo_all():
	lipo(False)
	lipo(True)


strip_all()
lipo_all()
common.create_macos_app(
	'misc/dist/ios_xcode',
	[
		'libgodot.iphone.debug.fat.a'
		'libgodot.iphone.release.fat.a'
	],
	'iphone', 'iphone.zip',
	common.get_templates_output_path(),
	[], '')
