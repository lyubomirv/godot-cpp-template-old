#!/usr/bin/env python3

import common

build_command = common.BuildCommand().javascript().tools(False).no_debug_symbols()

build_command_debug = build_command.release_debug()
common.exec_build_command(build_command_debug, common.Config.DOCKER_IMAGE_WEB)

build_command_release = build_command.release()
common.exec_build_command(build_command_release, common.Config.DOCKER_IMAGE_WEB)
