#!/usr/bin/env python3

import common
import os
from sys import platform


def get_exec_prefix():
	if platform == "win32":
		return '\\.'
	return './'


def get_gradlew_for_exec():
	return get_exec_prefix() + 'gradlew'


def get_config_command(is_release):
	command = common.BuildCommand().android()
	return command.release().no_debug_symbols() if is_release else command.debug()


def get_android_build_path():
	return os.path.normpath(
		os.path.join(
			common.get_godot_path(),
			'platform',
			'android',
			'java'))


def exec_gradlew_local():
	command = [
		get_gradlew_for_exec(),
		'generateGodotTemplates']
	common.exec_command(command, get_android_build_path())


def exec_gradlew_docker():
	docker_command = common.get_docker_command(
		[common.get_godot_path() + ':/godot'],
		common.Config.DOCKER_IMAGE_ANDROID,
		'./gradlew',
		['generateGodotTemplates'],
		'/godot/platform/android/java')
	common.exec_command(docker_command, '.')


def generate_templates():
	if common.Config.USE_DOCKER:
		exec_gradlew_docker()
	else:
		exec_gradlew_local()
		

docker_image = common.Config.DOCKER_IMAGE_ANDROID
common.exec_build_command(get_config_command(False).android_armv7(), docker_image)
common.exec_build_command(get_config_command(False).android_arm64v8(), docker_image)
common.exec_build_command(get_config_command(False).android_x86(), docker_image)
common.exec_build_command(get_config_command(False).android_x86_64(), docker_image)
common.exec_build_command(get_config_command(True).android_armv7(), docker_image)
common.exec_build_command(get_config_command(True).android_arm64v8(), docker_image)
common.exec_build_command(get_config_command(True).android_x86(), docker_image)
common.exec_build_command(get_config_command(True).android_x86_64(), docker_image)
generate_templates()
