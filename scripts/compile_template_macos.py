#!/usr/bin/env python3

import common


def get_config_command(is_release):
	command = common.BuildCommand().template().macos()
	return command.release().no_debug_symbols() if is_release else command.debug()


common.exec_build_command(get_config_command(False).macos_arm64())
common.exec_build_command(get_config_command(False).macos_x86_64())
common.exec_build_command(get_config_command(True).macos_arm64())
common.exec_build_command(get_config_command(True).macos_x86_64())
