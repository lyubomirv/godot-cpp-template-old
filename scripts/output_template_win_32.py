#!/usr/bin/env python3

import common


common.strip_and_copy_godot_products(
	['godot.windows.opt.32.exe', 'godot.windows.debug.32.exe'],
	common.get_templates_output_path(),
	common.Config.DOCKER_IMAGE_WINDOWS_64,
	['windows_32_release.exe', 'windows_32_debug.exe'])

