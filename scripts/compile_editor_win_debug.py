#!/usr/bin/env python3

import common

build_command = common.BuildCommand().windows().debug().bits_64()
common.exec_build_command(build_command, common.Config.DOCKER_IMAGE_WINDOWS)
