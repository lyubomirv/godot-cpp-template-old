#!/usr/bin/env python3

import common

build_command = common.BuildCommand().macos().tools(True).release_debug().no_debug_symbols()

build_command_x86_64 = build_command.macos_x86_64()
common.exec_build_command(build_command_x86_64)

build_command_arm64 = build_command.macos_arm64()
common.exec_build_command(build_command_arm64)
