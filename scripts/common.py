#!/usr/bin/env python3

import errno
import os
import shutil
import stat
import subprocess
import sys
import tempfile


class Config:
	BUILD_JOBS_COUNT = 4
	USE_DOCKER = True
	DOCKER_IMAGE_ANDROID = 'combined'
	DOCKER_IMAGE_LINUX_32 = 'combined'
	DOCKER_IMAGE_LINUX_64 = 'combined'
	DOCKER_IMAGE_WEB = 'web'
	DOCKER_IMAGE_WINDOWS = 'windows'
	OUTPUT_DIR = 'output'


def get_script_path():
	return os.path.dirname(os.path.realpath(sys.argv[0]))


def get_additional_arguments():
	return sys.argv[1:]


def get_base_path():
	base = os.path.join(get_script_path(), '..')
	return os.path.normpath(base)


def get_godot_path():
	return os.path.normpath(os.path.join(get_base_path(), 'godot'))


def get_godot_bin_path():
	return os.path.normpath(os.path.join(get_godot_path(), 'bin'))


def get_output_path():
	return os.path.normpath(os.path.join(get_base_path(), Config.OUTPUT_DIR))


def get_editor_output_path():
	return os.path.normpath(os.path.join(get_output_path(), 'editor'))


def get_templates_output_path():
	return os.path.normpath(os.path.join(get_output_path(), 'templates'))


def get_modules_path():
	return os.path.normpath(os.path.join(get_base_path(), 'src'))


def make_dir(dir):
	os.makedirs(dir, exist_ok = True)


def remove_file(filename):
	try:
		os.remove(filename)
	except OSError as e:
		if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
			raise # re-raise exception if a different error occurred


def copy_godot_product(filename, output_dir, renamed_filename = ''):
	final_filename = renamed_filename if renamed_filename else filename
	final_filename = os.path.normpath(os.path.join(output_dir, final_filename))
	make_dir(output_dir)

	message = 'Copying ' + filename + ' to ' + output_dir
	if renamed_filename:
		message += ' as ' + renamed_filename
	print(message)

	shutil.copy(
		os.path.normpath(os.path.join(get_godot_bin_path(), filename)),
		final_filename)


def strip_and_copy_godot_products(filenames, output_dir, docker_image = '', renamed_filenames = []):
	for i, filename in enumerate(filenames):
		exec_strip_command(filename, docker_image)
		final_filename = renamed_filenames[i] if len(renamed_filenames) > i else filename
		copy_godot_product(filename, output_dir, final_filename)


def exec_command(command, cwd):
	subprocess.run(command, cwd = cwd)


class BuildCommand:
	def __init__(self):
		self.__command = [self.__SCONS]
		self.jobs(Config.BUILD_JOBS_COUNT)

	def __str__(self):
		return ' '.join(self.__command)

	def command(self):
		return self.__command

	def template(self):
		return self.tools(False)
	
	def tools(self, value):
		return self.__append(self.__TOOLS_YES) if value else self.__append(self.__TOOLS_NO)

	def android(self):
		return self.__append(self.__PLATFORM_ANDROID)

	def ios(self):
		return self.__append(self.__PLATFORM_IOS)

	def javascript(self):
		return self.__append(self.__PLATFORM_JAVASCRIPT)

	def linux(self):
		return self.__append(self.__PLATFORM_LINUX)

	def macos(self):
		return self.__append(self.__PLATFORM_MACOS)

	def server(self):
		return self.__append(self.__PLATFORM_SERVER)

	def windows(self):
		return self.__append(self.__PLATFORM_WINDOWS)

	def android_armv7(self):
		return self.__append(self.__ANDROID_ARCH_ARMV7)

	def android_arm64v8(self):
		return self.__append(self.__ANDROID_ARCH_ARM64V8)

	def android_x86(self):
		return self.__append(self.__ANDROID_ARCH_X86)

	def android_x86_64(self):
		return self.__append(self.__ANDROID_ARCH_X86_64)

	def ios_arm(self):
		return self.__append(self.__IOS_ARCH_ARM)

	def ios_arm64(self):
		return self.__append(self.__IOS_ARCH_ARM64)

	def ios_x86_64(self):
		return self.__append(self.__IOS_ARCH_X86_64).__append(self.__IOS_SIMULATOR)

	def macos_arm64(self):
		return self.__append(self.__MACOS_ARCH_ARM64)

	def macos_x86_64(self):
		return self.__append(self.__MACOS_ARCH_X86_64)

	def debug(self):
		return self.__append(self.__TARGET_DEBUG)

	def release(self):
		return self.__append(self.__TARGET_RELEASE)

	def release_debug(self):
		return self.__append(self.__TARGET_RELEASE_DEBUG)
	
	def no_debug_symbols(self):
		return self.__append(self.__DEBUG_SYMBOLS_NO)

	def bits_32(self):
		return self.__append(self.__BITS_32)

	def bits_64(self):
		return self.__append(self.__BITS_64)
	
	def jobs(self, n):
		return self.__append('-j' + str(n))

	def add_arguments(self, arguments):
		return self.__append(arguments)

	def __append(self, param):
		self.__command.append(param)
		return self

	__SCONS = 'scons'
	__TOOLS_NO = 'tools=no'
	__TOOLS_YES = 'tools=yes'
	__PLATFORM_ANDROID = 'platform=android'
	__PLATFORM_IOS = 'platform=iphone'
	__PLATFORM_JAVASCRIPT = 'platform=javascript'
	__PLATFORM_LINUX = 'platform=x11'
	__PLATFORM_MACOS = 'platform=osx'
	__PLATFORM_SERVER = 'platform=server'
	__PLATFORM_WINDOWS = 'platform=windows'
	__ANDROID_ARCH_ARMV7 = 'android_arch=armv7'
	__ANDROID_ARCH_ARM64V8 = 'android_arch=arm64v8'
	__ANDROID_ARCH_X86 = 'android_arch=x86'
	__ANDROID_ARCH_X86_64 = 'android_arch=x86_64'
	__IOS_ARCH_ARM = 'arch=arm'
	__IOS_ARCH_ARM64 = 'arch=arm64'
	__IOS_ARCH_X86_64 = 'arch=x86_64'
	__IOS_SIMULATOR = 'ios_simulator=yes'
	__MACOS_ARCH_ARM64 = 'arch=arm64'
	__MACOS_ARCH_X86_64 = 'arch=x86_64'
	__TARGET_DEBUG = 'target=debug'
	__TARGET_RELEASE = 'target=release'
	__TARGET_RELEASE_DEBUG = 'target=release_debug'
	__DEBUG_SYMBOLS_NO = 'debug_symbols=no'
	__BITS_32 = 'bits=32'
	__BITS_64 = 'bits=64'


def get_docker_command(volumes, docker_image, command, arguments = [], workdir = ''):
	assembled_command = ['docker', 'run', '-t', '--rm']
	for volume in volumes:
		assembled_command += ['-v', volume]
	if workdir:
		assembled_command += ['-w', workdir]
	assembled_command.append(docker_image)
	if isinstance(command, list):
		assembled_command += command
	else:
		assembled_command.append(command)
	assembled_command += arguments
	return assembled_command


def exec_build_command_docker(build_command, docker_image):
	docker_command = get_docker_command(
		[get_godot_path() + ':/godot', get_modules_path() + ':/modules'],
		docker_image,
		build_command.command(),
		['custom_modules=/modules'],
		'/godot')
	exec_command(docker_command, '.')


def exec_build_command_local(build_command):
	command = build_command.command()
	command += ['custom_modules=' + get_modules_path()]
	command += get_additional_arguments()
	exec_command(command, get_godot_path())


def exec_build_command(build_command, docker_image = ''):
	if Config.USE_DOCKER:
		exec_build_command_docker(build_command, docker_image)
	else:
		exec_build_command_local(build_command)


def exec_strip_command_docker(filename, docker_image):
	docker_command = get_docker_command(
		[get_godot_bin_path() + ':/output'],
		docker_image,
		'strip',
		['-S', '-x', '/output/' + filename])
	exec_command(docker_command, '.')


def exec_strip_command_local(filename):
	command = ['strip', '-S', '-x', filename]
	exec_command(command, get_godot_bin_path())


def exec_strip_command(filename, docker_image = ''):
	print('Stripping ' + filename)
	if Config.USE_DOCKER:
		exec_strip_command_docker(filename, docker_image)
	else:
		exec_strip_command_local(filename)


def exec_lipo_command(src_filenames, dest_filename):
	print('Running lipo to get ' + dest_filename)
	command = ['lipo', '-create']
	for src_filename in src_filenames:
		command.append(src_filename)
	command += ['-output', dest_filename]
	exec_command(command, get_godot_bin_path())


def create_macos_app(app_template, executable_filenames, output_app_filename, output_zip_filename, output_path, renamed_filenames = [], binary_dir = 'Contents/MacOS'):
	tempdir = tempfile.gettempdir()
	template_work = os.path.normpath(os.path.join(tempdir, output_app_filename))
	shutil.rmtree(template_work, True)

	print('Creating app in ' + template_work)

	remove_file(os.path.join(tempdir, output_zip_filename))

	godot_template = os.path.normpath(os.path.join(get_godot_path(), app_template))
	shutil.copytree(godot_template, template_work)

	binary_dir_absolute = os.path.normpath(os.path.join(template_work, binary_dir))
	make_dir(binary_dir_absolute)

	for i, executable_filename in enumerate(executable_filenames):
		filename_in_bundle = renamed_filenames[i] if len(renamed_filenames) > i else executable_filename
		filename_in_bundle_path = os.path.normpath(os.path.join(binary_dir_absolute, filename_in_bundle))
		shutil.copy(os.path.normpath(os.path.join(get_godot_bin_path(), executable_filename)), filename_in_bundle_path)
		st = os.stat(filename_in_bundle_path)
		os.chmod(filename_in_bundle_path, st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)

	print('Zipping app')
	zip_command = ['zip', '-r', output_zip_filename, output_app_filename]
	exec_command(zip_command, tempdir)

	make_dir(output_path)
	output_zip_filename_absolute = os.path.normpath(os.path.join(output_path, output_zip_filename))
	shutil.copy(
		os.path.join(tempdir, output_zip_filename),
		output_zip_filename_absolute)
	
	print('App created: ' + output_zip_filename_absolute)
