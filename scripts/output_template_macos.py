#!/usr/bin/env python3

import common


def strip_impl(is_release):
	mode = 'opt' if is_release else 'debug'
	common.exec_strip_command('godot.osx.' + mode + '.arm64')
	common.exec_strip_command('godot.osx.' + mode + '.x86_64')


def strip():
	strip_impl(False)
	strip_impl(True)


strip()
common.exec_lipo_command(['godot.osx.debug.arm64', 'godot.osx.debug.arm64'], 'godot.osx.debug.universal')
common.exec_lipo_command(['godot.osx.opt.arm64', 'godot.osx.opt.arm64'], 'godot.osx.opt.universal')
common.create_macos_app(
	'misc/dist/osx_template.app',
	['godot.osx.debug.universal', 'godot.osx.opt.universal'],
	'osx_template.app', 'osx.zip',
	common.get_templates_output_path(),
	['godot_osx_debug.64', 'godot_osx_release.64'])
