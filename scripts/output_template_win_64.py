#!/usr/bin/env python3

import common


common.strip_and_copy_godot_products(
	['godot.windows.opt.64.exe', 'godot.windows.debug.64.exe'],
	common.get_templates_output_path(),
	common.Config.DOCKER_IMAGE_WINDOWS_64,
	['windows_64_release.exe', 'windows_64_debug.exe'])

