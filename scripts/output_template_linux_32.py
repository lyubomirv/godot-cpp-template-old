#!/usr/bin/env python3

import common


common.strip_and_copy_godot_products(
	['godot.x11.opt.32', 'godot.x11.debug.32'],
	common.get_templates_output_path(),
	common.Config.DOCKER_IMAGE_LINUX_32,
	['linux_x11_32_release', 'linux_x11_32_debug'])

