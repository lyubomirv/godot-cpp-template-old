extends Node2D

var rng = RandomNumberGenerator.new()

onready var icon = get_node('icon') as Sprite

func _ready():
	rng.randomize()
	# testCustomModule()

func _input(event):
	if event is InputEventKey || event is InputEventAction || event is InputEventMouseButton || event is InputEventJoypadButton || event is InputEventScreenTouch:
		changeIconColour()

func changeIconColour():
	var r = rng.randf_range(0.0, 1.0)
	var g = rng.randf_range(0.0, 1.0)
	var b = rng.randf_range(0.0, 1.0)
	icon.modulate = Color(r, g, b)

func testCustomModule():
	var s = Summator.new()
	s.add(5)
	s.add(3)
	print('--- Summator result: ' + str(s.get_total()))
