# godot-cpp-template

Template project for creating a game in C++ using Godot

## What is this

This is a template project for using the Godot engine that should be helpful for anyone who wants to:
- Create a game using C++
- Modify or extend the Godot engine to add any needed functionality for their particular game
- Integrate additional modules found on the Internet, like ad libraries

The goal is to make things easier for non-developers (for the case of integrating modules) while also allowing for full control of the code for the people that need it.

**Supported platforms:** Android, iOS, Linux, MacOS (includes ARM64 support), Windows (uses `mingw`), Web. C#/Mono is **NOT** supported.

Note that building the editor/templates and exporting for iOS and MacOS requires an actual MacOS machine (or an alternative solution) and cannot be done using `docker`.

Note that building the editor for Linux/Windows will build it only in the 64-bit variant. You can still build and export 32-bit templates, though.
 
---

## How to use

Checkout the project including the externals, in order to also get Godot's code.

From there, you will need to follow this procedure:
1. Build the Godot editor (every time you change a C++ file or add/modify a module)
2. Build the templates for the platforms you care about and (preferably) place them in the standard templates location
3. Use the editor you built to make your game (programming, assets, etc.)
4. Export your game for the platforms you want using the templates you built (if yoo've replaced the standard ones, you do not need to do anything special here)

If all you want is to integrate some pre-made module, you'll probably need to go through steps 1 and 2 just once, and then go on to making your game in the regular way, just using your editor and your templates. If you already have a project, put it in the `project` folder - you can replace everything but keep the `export` folder.

## Prerequisites

This is what you will need to have installed:
- Python 3
- Either `docker` or the specific Godot dependencies (read below for details)

Using this project means you will have to build the Godot engine yourself, and this is exactly what it helps with. There are two ways you can go about building the engine using this template. In both cases, the scripts provided in this repository will help with the building.

---

## A note about building

There are two supported ways for building the game for most of the platforms

#### Building using `docker`

Using [`docker`](https://www.docker.com) is by far the easier and the recommended way to use this project template. This way you avoid installing all the specific software dependencies that Godot needs. However, it is currently not possible to build for iOS and MacOS using docker, so for those platforms, you will still need a Mac (or an alternative solution) where you need to install all dependencies.

The docker images that are used for building the project must be built first. Their files are in the `docker` directory. Just use the `setup` script there to build them.

NOTE: The docker files are copied from [this other repository](https://gitlab.com/bluabito/godot-docker).

#### Building directly on your machine

This means you will have to install all the required software for building Godot as explained in their documentation, for all the platforms that you care about. Refer to the documentation for this. However, it is highly recommended that you use `docker`. For active development, you should probably still install the needed dependencies, only for the platform you develop on. Especially if you want to use a debugger.

## Building

The folder `scripts` contains a bunch of easy-to-use python scripts that can be used to build the editor and templates for all the platforms. The scripts do not need any arguments and when run, place their results in a folder `output`.

The first thing that needs to be done is to open the file `scripts/common.py` and edit the parameters in the class `Config` - there you can specify whether you are using `docker` and the docker images for each platform, if different.

Then you can simply execute any of the scripts you need. For every platform, first use a script for compiling, and then the corresponging script for outputting. Example for Android (assuming you are in the base directory):

    ./scripts/compile_template_android.py
    ./scripts/output_template_android.py

Then you will find the Android template files in `output/templates`. You can use the same procedure for all the platforms.

If all you want is to integrate some pre-made module, you need to do the above for the editor (for your platform only) and then for the templates (for the platforms you want to export to).

If you are planning to seriously use C++ for your game, you can integrate those scripts in your IDE / build environment. They are already integrated for VS Code (see below).

---

## Automatic builds using Buildbot

_If your goal is to build Godot only once or twice (like to integrate a module), you can skip this section._

For anyone considering seriously using C++ with Godot, setting up an automatic build system is very important. This project includes a Buildbot configuration that is supposed to be set up on an always-on machine, preferably running Linux. It depends on `docker` (and a MacOS machine). For instructions, please refer to the [README.md](buildbot/README.md) in the folder `buildbot`.

---

## VS Code

If using VS Code, the scripts for building the editor and templates can be easily used from inside the editor itself. There are `tasks.json` and `launch.json` files provided in the directory `.vscode` that make it easy to build and execute the editor / game and even debug. However, in order to be able to debug, you will need to install a debugger for your development platform (gdb for Linux/Windows, lldb for MacOS).

---

## Platform-specific instructions

### Android

In order to export the game for Android, you need to first generate debug and release keystores. These are files that are needed for signing the Android app packages. They can be created on the command-line by using the Android SDK directly or by using the docker image, if the Android SDK is not present. Here is how it can be done using docker (on linux, for windows the `$PWD` part may be different):

Debug (replace the password if wanted):

    docker run -it -v $PWD:/temp-dir -w /temp-dir combined keytool -keyalg RSA -genkeypair -alias androiddebugkey -keypass android -keystore debug.keystore -storepass android -dname "CN=Android Debug,O=Android,C=US" -validity 9999 -deststoretype pkcs12

Then move the generated file `debug.keystore` to `project/export/android` and replace the default one there. You might also need to open the Export Settings in the editor and change the 'Debug Keystore Pass' in the android export 

Release (change the password):

    docker run -it -v $PWD:/temp-dir -w /temp-dir combined keytool -v -genkey -v -keystore release.keystore -alias androidreleasekey -storepass android -storetype PKCS12 -keyalg RSA -validity 10000

This will ask you for a few things which you should provide. Somewhere near the end you should input 'yes' to confirm.
Then move the generated file `release.keystore` to `project/export/android` and replace the default one there.

If using `docker` to generate those files, you might need to change their owner, as at least on Linux it comes out as `root`.

Please refer to the documentation about Android exports [here](https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_for_android.html).


### iOS and MacOS

For these platforms it is not possible to build the editor and templates using `docker`. If you are interested in building your game for iOS or MacOS, or maybe even developing on MacOS, you will need to install the dependencies as described in Godot's documentation. This needs to be done for using Buildbot too.

Apart from that inconvenience, in the directory `scripts` there are scripts for iOS and MacOS that make it just as easy to build for those platforms, as for the others.

One important note: Exporting for iOS will not work unless you have an Apple developer account and whatever else needed for publishing to the AppStore. Look at the project settings for you game in the editor - there are fields that need to be filled with information about your Apple account, etc.
