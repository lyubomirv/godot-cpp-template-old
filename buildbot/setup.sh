#!/bin/bash

docker build -t buildbot-master -f ./docker/Dockerfile.master ./docker
docker build -t buildbot-worker -f ./docker/Dockerfile.worker ./docker

echo "@reboot root chmod 666 /var/run/docker.sock" | sudo tee /etc/cron.d/dockersock
