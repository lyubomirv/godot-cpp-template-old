@echo off

docker build -t buildbot-master -f .\docker\Dockerfile.master .\docker
docker build -t buildbot-worker -f .\docker\Dockerfile.worker .\docker
