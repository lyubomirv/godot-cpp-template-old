### Instructions

1. Get a device with MacOS, or a VM like [this](https://github.com/foxlet/macOS-Simple-KVM) or something
2. If it is a VM, make it run with the host's boot, like by creating a file in `/etc/cron.d` and using `@reboot`
3. On the MacOS, install:
    - `XCode`
    - `scons`. Scons can be installed with `pip3 install scons`
    - `buildbot-worker`. It can be installed with `pip3 install buildbot-worker`. If there is an error, python needs to be updated
4. Make the buildbot worker start with the OS, by doing the necessary changes in the file `buildbot.worker.plist` and putting it in `/Library/LaunchDaemons`.
