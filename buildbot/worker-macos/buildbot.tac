import os
import sys

from twisted.application import service
from twisted.python.log import FileLogObserver
from twisted.python.log import ILogObserver

from buildbot_worker.bot import Worker

# setup worker
basedir = os.path.abspath(os.path.dirname(__file__))
application = service.Application('buildbot-worker')


application.setComponent(ILogObserver, FileLogObserver(sys.stdout).emit)
# and worker on the same process!
buildmaster_host = '192.168.10.2'
port = 9989
workername = 'worker_macos'
passwd = 'pass_macos'

keepalive = 600
umask = None
maxdelay = 300
allow_shutdown = None
maxretries = 10
delete_leftover_dirs = False

s = Worker(buildmaster_host, port, workername, passwd, basedir,
		   keepalive, umask=umask, maxdelay=maxdelay,
		   allow_shutdown=allow_shutdown, maxRetries=maxretries,
		   delete_leftover_dirs=delete_leftover_dirs)
s.setServiceParent(application)
