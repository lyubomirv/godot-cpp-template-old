#### Prerequisites

1. You will need to set up an ssh key for the buildbot system. Generate one as usual and use the files to fill the contents of `docker/ssh`. The files there must not be renamed, just replace what is needed in their contents.
2. The files `.env` and `.env.container` contain environment variables that are used in the services - make sure to set those according to your project.
3. Proceed to setup

#### Setup

On Linux (tested on Ubuntu), run `setup.sh` in order to build the required docker images with the ssh keys and prepare other things. On other systems, please take a look at the script and do what is needed. It is not much.

#### Running

Run `restart.sh` or `restart.bat` depending on your host system. The script will first stop the containers, in case they are already running.

#### Output

The built files (editor/templates) by default will be placed in the directory `output` which is automatically created in this directory. On Linux, their owner will be the default user (1000:1000). Both of these and other settings can be changed in the files `.env` and `.env.container`.

#### How this works

There are two docker containers that are being run together. They are built from the official images of `buildbot-master` and `buildbot-worker`. The reason for building on top of them is to handle some file permissions regarding the ssh key files, and to install any additionally needed packages.

The worker then runs the build commands for godot using the docker instance of the host computer. To achieve this, the docket socket `/var/run/docker.sock` and the `docker` executable are mouted as volumes in the worker container.

However, in some cases there is a problem with this approach, as the `docker.sock` will not allow to be used from inside a container due to its file permissions. To fix (or rather workaround) it, the permissions of the socket file could simply be changed. However, docker changes them back after a reboot of the host system. And to fix that, the permissions of the file need to be changed on every reboot. For Linux, this is done using `cron` as part of the script `setup.sh`, but there are plenty of other ways if that won't work on your system.
This issue is likely present only on Linux and that is why the 'fix' is applied only in the Linux' setup script.
