#!/bin/sh

docker run -t --rm -v /dev/bus/usb:/dev/bus/usb -v $PWD:/project -w /project combined /android/sdk/platform-tools/adb "$@"
