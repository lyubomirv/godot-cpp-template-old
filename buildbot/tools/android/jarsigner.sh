#!/bin/sh

docker run -t --rm -v /tmp/cache:/tmp/cache -v $HOST_WORKER_EXPORT_BUILDER_DIR/build/project:/project -w /project combined jarsigner "$@"
