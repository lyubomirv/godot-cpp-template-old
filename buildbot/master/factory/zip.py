from buildbot.plugins import *

from factory.util.util import create_factory_and_get_revision
from factory.util.util import get_output_dir_prop

def zip_templates():
	factory = create_factory_and_get_revision()
	factory.addStep(
		steps.MasterShellCommand(
			command = util.Interpolate('cd ' + get_output_dir_prop() + ' && zip -r templates templates')))
	return factory
