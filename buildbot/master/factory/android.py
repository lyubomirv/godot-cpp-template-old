from buildbot.plugins import *

from factory.util.build_command import BuildCommand
from factory.util.config import Config
from factory.util.util import add_files_upload_steps
from factory.util.util import create_factory_and_checkout
from factory.util.util import get_compile_step_docker
from factory.util.util import get_docker_command
from factory.util.util import get_host_godot_dir

BUILDER = 'android-template'
DOCKER_IMAGE = Config().docker_image_android

def get_build_command():
	return BuildCommand().android()

def add_template_steps(factory, build_command, is_release):
	build_command = build_command.release().no_debug_symbols() if is_release else build_command.debug()
	factory.addStep(get_compile_step_docker(BUILDER, DOCKER_IMAGE, build_command.command()))

def get_generate_templates_step():
	command = get_docker_command(
		[get_host_godot_dir(BUILDER) + ':/godot'],
		DOCKER_IMAGE,
		'./gradlew',
		['generateGodotTemplates'],
		'/godot/platform/android/java')
	return steps.ShellCommand(
		command = command,
		haltOnFailure = True,
		description = 'generating templates', descriptionDone = 'generate templates')

def templates():
	factory = create_factory_and_checkout()

	add_template_steps(factory, get_build_command().android_armv7(), False)
	add_template_steps(factory, get_build_command().android_armv7(), True)
	add_template_steps(factory, get_build_command().android_arm64v8(), False)
	add_template_steps(factory, get_build_command().android_arm64v8(), True)
	add_template_steps(factory, get_build_command().android_x86(), False)
	add_template_steps(factory, get_build_command().android_x86(), True)
	add_template_steps(factory, get_build_command().android_x86_64(), False)
	add_template_steps(factory, get_build_command().android_x86_64(), True)
	factory.addStep(get_generate_templates_step())

	add_files_upload_steps(factory, ['android_debug.apk', 'android_release.apk', 'android_source.zip'], 'templates')

	return factory
