from buildbot.plugins import *

from factory.util.build_command import BuildCommand
from factory.util.config import Config
from factory.util.util import add_desktop_template_steps
from factory.util.util import add_files_upload_steps
from factory.util.util import create_editor_factory
from factory.util.util import create_factory_and_checkout
from factory.util.util import get_compile_step_docker
from factory.util.util import get_strip_step_docker

def get_build_command():
	return BuildCommand().linux()

def editor_64():
	return create_editor_factory('linux-editor-64', Config().docker_image_linux_64, get_build_command(), 'godot.x11.opt.tools.64')

def templates():
	factory = create_factory_and_checkout()

	config = Config()
	builder = 'linux-template'
	filename_base = 'godot.x11'
	filename_final_base = 'linux_x11_'
	add_desktop_template_steps(factory, builder, config.docker_image_linux_64, get_build_command(), 64, False, filename_base, '', filename_final_base + '64_debug')
	add_desktop_template_steps(factory, builder, config.docker_image_linux_64, get_build_command(), 64, True, filename_base, '', filename_final_base + '64_release')
	add_desktop_template_steps(factory, builder, config.docker_image_linux_32, get_build_command(), 32, False, filename_base, '', filename_final_base + '32_debug')
	add_desktop_template_steps(factory, builder, config.docker_image_linux_32, get_build_command(), 32, True, filename_base, '', filename_final_base + '32_release')

	return factory

def headless():
	factory = create_factory_and_checkout()

	builder = 'linux-headless'
	docker_image = Config().docker_image_linux_64
	filename = 'godot_server.x11.opt.tools.64'
	factory.addStep(
		get_compile_step_docker(
			builder,
			docker_image,
			BuildCommand().server().tools(True).release_debug().no_debug_symbols().bits_64().command()))
	factory.addStep(get_strip_step_docker(builder, docker_image, filename))
	add_files_upload_steps(factory, [filename], 'headless', True)
	return factory
