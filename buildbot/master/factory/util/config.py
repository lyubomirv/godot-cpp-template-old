import os

class Config:
	def __init__(self):
		self.worker_name = os.environ.get('BUILDBOT_WORKER_NAME', 'worker')
		self.worker_pass = os.environ.get('BUILDBOT_WORKER_PASS', 'pass')
		self.worker_macos_name = os.environ.get('BUILDBOT_WORKER_MACOS_NAME', 'worker_macos')
		self.worker_macos_pass = os.environ.get('BUILDBOT_WORKER_MACOS_PASS', 'pass_macos')
		self.worker_port = int(os.environ.get('BUILDBOT_WORKER_PORT', 9989))
		self.web_url = os.environ.get('BUILDBOT_WEB_URL', 'http://localhost:8010/')
		self.web_port = int(os.environ.get('BUILDBOT_WEB_PORT', 8010))
		self.web_title = os.environ.get('BUILDBOT_WEB_TITLE', 'Set your title in .env.container')
		self.web_title_url = os.environ.get('BUILDBOT_WEB_TITLE_URL', 'Set your title URL in .env.container')
		self.git_project_url = os.environ.get('GIT_PROJECT_URL', 'ERROR: No project git URL set in .env.container')
		self.build_jobs_count = int(os.environ.get('BUILD_JOBS_COUNT', 'ERROR" No jobs count (number of threads for compiling) set in .env.container'))
		self.final_user_id = int(os.environ.get('FINAL_USER_ID', 1000))
		self.final_group_id = int(os.environ.get('FINAL_GROUP_ID', 1000))
		self.host_worker_dir = os.environ.get('HOST_WORKER_DIR', 'ERROR" No host worker dir set in .env')
		self.build_editor_linux = int(os.environ.get('BUILD_EDITOR_LINUX', 1)) == 1
		self.build_editor_macos = int(os.environ.get('BUILD_EDITOR_MACOS', 1)) == 1
		self.build_editor_windows = int(os.environ.get('BUILD_EDITOR_WINDOWS', 1)) == 1
		self.build_template_android = int(os.environ.get('BUILD_TEMPLATE_ANDROID', 1)) == 1
		self.build_template_ios = int(os.environ.get('BUILD_TEMPLATE_IOS', 1)) == 1
		self.build_template_linux = int(os.environ.get('BUILD_TEMPLATE_LINUX', 1)) == 1
		self.build_template_macos = int(os.environ.get('BUILD_TEMPLATE_MACOS', 1)) == 1
		self.build_template_web = int(os.environ.get('BUILD_TEMPLATE_WEB', 1)) == 1
		self.build_template_windows = int(os.environ.get('BUILD_TEMPLATE_WINDOWS', 1)) == 1
		self.export_game = int(os.environ.get('EXPORT_GAME', 1)) == 1
		self.export_game_ios = int(os.environ.get('EXPORT_GAME_IOS', 1)) == 1
		self.docker_image_android = os.environ.get('DOCKER_IMAGE_ANDROID', 'combined')
		self.docker_image_linux_32 = os.environ.get('DOCKER_IMAGE_LINUX_32', 'combined')
		self.docker_image_linux_64 = os.environ.get('DOCKER_IMAGE_LINUX_64', 'combined')
		self.docker_image_web = os.environ.get('DOCKER_IMAGE_WEB', 'web')
		self.docker_image_windows = os.environ.get('DOCKER_IMAGE_WINDOWS', 'windows')
	
	def is_any_template_build(self):
		return (self.build_template_android
			or self.build_template_ios
			or self.build_template_linux
			or self.build_template_macos
			or self.build_template_web
			or self.build_template_windows)
