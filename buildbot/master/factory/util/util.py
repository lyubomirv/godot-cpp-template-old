import os

from buildbot.plugins import *

from .config import Config
from .build_command import BuildCommand

# Global variables
OUTPUT_DIR = '/output'
OUTPUT_DIR_PROPERTY_NAME = 'output_dir'


def get_host_build_dir(builder):
	config = Config()
	return os.path.normpath(os.path.join(config.host_worker_dir, builder, 'build'))

def get_host_godot_dir(builder):
	build_dir = get_host_build_dir(builder)
	return os.path.normpath(os.path.join(build_dir, 'godot'))

def get_godot_output_dir():
	return os.path.normpath(os.path.join('godot', 'bin'))

def get_output_dir_prop():
	return '%(prop:' + OUTPUT_DIR_PROPERTY_NAME + ')s'

def create_factory_and_checkout(submodules = True):
	config = Config()
	
	factory = util.BuildFactory()
	factory.addStep(
		steps.Git(
			repourl = config.git_project_url,
			mode = 'incremental',
			submodules = submodules,
			branch = util.Property('branch', default = 'master'),
			alwaysUseLatest = True,
			progress = True))
	factory.addStep(
		steps.SetProperty(
			property = OUTPUT_DIR_PROPERTY_NAME,
			value = util.Interpolate(OUTPUT_DIR + '/%(prop:got_revision)s')))
	return factory

# Ths use_single_command is a workaround for a macos worker, as
# apparently there is a problem when using a pipe (|) - the
# command wraps the pipe in single quotes and makes it unusable
def create_factory_and_get_revision(use_single_command = False):
	config = Config()

	factory = util.BuildFactory()

	if use_single_command:
		factory.addStep(
			steps.SetPropertyFromCommand(
				command = util.Interpolate('git ls-remote --refs ' + config.git_project_url + ' %(prop:branch:-master)s | cut -f1'),
				property = 'got_revision',
				haltOnFailure = True,
				description = 'getting revision', descriptionDone = 'get revision'))
	else:
		factory.addStep(
			steps.SetPropertyFromCommand(
				command = [
					'git', 'ls-remote', '--refs', config.git_project_url, util.Property('branch', default = 'master'),
					'|', 'cut', '-f1'],
				property = 'got_revision',
				haltOnFailure = True,
				description = 'getting revision', descriptionDone = 'get revision'))

	factory.addStep(
		steps.SetProperty(
			property = OUTPUT_DIR_PROPERTY_NAME,
			value = util.Interpolate(OUTPUT_DIR + '/%(prop:got_revision)s')))
	return factory

def get_docker_command(volumes, docker_image, command, arguments = [], workdir = ''):
	assembled_command = ['docker', 'run', '-t', '--rm']
	for volume in volumes:
		assembled_command += ['-v', volume]
	if workdir:
		assembled_command += ['-w', workdir]
	assembled_command.append(docker_image)
	assembled_command.append(command)
	assembled_command += arguments
	return assembled_command

def get_docker_build_command(builder, docker_image, command):
	build_dir = get_host_build_dir(builder)
	godot_dir = get_host_godot_dir(builder)
	src_dir = os.path.normpath(os.path.join(build_dir, 'src'))
	return get_docker_command(
		[godot_dir + ':/godot', src_dir + ':/modules'],
		docker_image,
		command,
		['custom_modules=/modules'])

def get_docker_strip_command(builder, docker_image, filename):
	godot_dir = get_host_godot_dir(builder)
	output_dir = os.path.normpath(os.path.join(godot_dir, 'bin'))
	return get_docker_command(
		[output_dir + ':/output'],
		docker_image,
		'strip',
		['-S', '-x', '/output/' + filename])

def get_compile_step(build_command, workdir = 'build'):
	return steps.Compile(
		command = build_command,
		workdir = workdir,
		haltOnFailure = True,
		description = 'compiling', descriptionDone = 'compile')

def get_compile_step_docker(builder, docker_image, build_command):
	docker_command = get_docker_build_command(builder, docker_image, build_command)
	return get_compile_step(docker_command)

def get_strip_step(filename):
	return steps.ShellCommand(
		command = ['strip', '-S', '-x', filename],
		haltOnFailure = True,
		description = 'stripping', descriptionDone = 'strip')

def get_strip_step_docker(builder, docker_image, filename):
	docker_command = get_docker_strip_command(builder, docker_image, filename)
	return steps.ShellCommand(command = docker_command, description = 'stripping', descriptionDone = 'strip')

def get_lipo_step(filenames, output_filename):
	command = ['lipo', '-create'] + filenames + ['-output', output_filename]
	workdir = os.path.normpath(os.path.join('build', get_godot_output_dir()))
	return steps.ShellCommand(
		command = command,
		workdir = workdir,
		description = 'lipo', descriptionDone = 'lipo')

def get_master_rename_step(directory, src, dest):
	return steps.MasterShellCommand(
		command = util.Interpolate('cd ' + get_output_dir_prop() + '/' + directory + ' && mv ' + src + ' ' + dest))

def add_desktop_template_steps(factory, builder, docker_image, platform_build_command, bits, is_release, filename_base, filename_ext, final_filename):
	build_command = platform_build_command.template()
	build_command = build_command.release().no_debug_symbols() if is_release else build_command.debug()
	build_command = build_command.bits_64() if bits == 64 else build_command.bits_32()
	factory.addStep(get_compile_step_docker(builder, docker_image, build_command.command()))

	filename = filename_base
	filename += '.'
	filename += 'opt' if is_release else 'debug'
	filename += '.'
	filename += str(bits)
	if filename_ext:
		filename += '.' + filename_ext

	factory.addStep(get_strip_step_docker(builder, docker_image, filename))
	add_files_upload_steps(factory, [filename], 'templates')
	factory.addStep(get_master_rename_step('templates', filename, final_filename))

def add_ensure_master_dir_exists_step(factory, master_directory):
	factory.addStep(
		steps.MasterShellCommand(
			command = util.Interpolate('mkdir -p ' + get_output_dir_prop() + '/' + master_directory)))

def add_chown_master_dir_step(factory):
	config = Config()
	user_group = str(config.final_user_id) + ':' + str(config.final_group_id)
	chown_dir_command_str = 'chown -R ' + user_group + ' ' + get_output_dir_prop()
	factory.addStep(
		steps.MasterShellCommand(
			command = util.Interpolate(chown_dir_command_str)))

def add_file_upload_steps(factory, filename, worker_directory, master_directory, is_executable = False):
	def get_file_upload_step(filename, worker_directory, master_directory, is_executable = False):
		output_directory_prop = get_output_dir_prop()
		if master_directory:
			output_directory_prop = get_output_dir_prop() + '/' + master_directory

		filename_worker = os.path.normpath(os.path.join(worker_directory, filename))
		return steps.FileUpload(
			workersrc = filename_worker,
			masterdest = util.Interpolate(output_directory_prop + '/' + filename),
			mode = 0o755 if is_executable else 0o644,
			url = 'todo-file')

	add_ensure_master_dir_exists_step(factory, master_directory)

	# Upload the file and set its owner
	factory.addStep(get_file_upload_step(filename, worker_directory, master_directory, is_executable))
	add_chown_master_dir_step(factory)

def add_files_upload_steps(factory, filenames, master_directory, is_executable = False):
	for filename in filenames:
		add_file_upload_steps(factory, filename, get_godot_output_dir(), master_directory, is_executable)

def get_file_download_step(filename_master, filename_worker, master_output_directory = '', is_executable = False):
	output_directory_prop = get_output_dir_prop()
	if master_output_directory:
		output_directory_prop = get_output_dir_prop() + '/' + master_output_directory

	return steps.FileDownload(
		mastersrc = util.Interpolate(output_directory_prop + '/' + filename_master),
		workerdest = filename_worker,
		mode = 0o755 if is_executable else 0o644)

def get_chown_step(path, user, group):
	return steps.MasterShellCommand(
		command = ['chown', '-R', str(user) + ':' + str(group), path])

def create_editor_factory(builder, docker_image, platform_build_command, filename):
	factory = create_factory_and_checkout()
	factory.addStep(get_compile_step_docker(builder, docker_image, platform_build_command.release_debug().no_debug_symbols().bits_64().command()))
	factory.addStep(get_strip_step_docker(builder, docker_image, filename))
	add_files_upload_steps(factory, [filename], 'editor', True)
	return factory
