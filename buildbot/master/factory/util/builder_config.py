import os

from buildbot.plugins import *

from factory import android
from factory import export
from factory import ios
from factory import linux
from factory import macos
from factory import web
from factory import windows
from factory import zip
from factory.util.config import Config

class BuilderConfig:
	def __init__(self):
		self._config = Config()

	def any_branch_builders(self):
		builders = []
		if self._config.build_editor_linux:
			builders.append(self._EDITOR_LINUX)
		elif self._config.build_template_linux:
			builders.append(self._TEMPLATE_LINUX)
		if self._config.build_editor_macos:
			builders.append(self._EDITOR_MACOS)
		elif self._config.build_template_macos:
			builders.append(self._TEMPLATE_MACOS)
		if self._config.build_editor_windows:
			builders.append(self._EDITOR_WINDOWS)
		elif self._config.build_template_windows:
			builders.append(self._TEMPLATE_WINDOWS)
		if self._config.build_template_android:
			builders.append(self._TEMPLATE_ANDROID)
		if self._config.build_template_ios:
			builders.append(self._TEMPLATE_IOS)
		if self._config.build_template_web:
			builders.append(self._TEMPLATE_WEB)
		return builders

	def force_builders(self):
		builders = self.nightly_builders()
		if self._config.is_any_template_build():
			builders.append(self._ZIP_TEMPLATES)
			if self._config.export_game:
				builders.append(self._EXPORT_GAME)
				if self._config.export_game_ios:
					builders.append(self._EXPORT_GAME_IOS)
		return builders

	def nightly_builders(self):
		builders = []
		if self._config.build_editor_linux:
			builders.append(self._EDITOR_LINUX)
		if self._config.build_editor_macos:
			builders.append(self._EDITOR_MACOS)
		if self._config.build_editor_windows:
			builders.append(self._EDITOR_WINDOWS)
		builders.append(self._HEADLESS_LINUX)
		if self._config.build_template_android:
			builders.append(self._TEMPLATE_ANDROID)
		if self._config.build_template_ios:
			builders.append(self._TEMPLATE_IOS)
		if self._config.build_template_linux:
			builders.append(self._TEMPLATE_LINUX)
		if self._config.build_template_macos:
			builders.append(self._TEMPLATE_MACOS)
		if self._config.build_template_web:
			builders.append(self._TEMPLATE_WEB)
		if self._config.build_template_windows:
			builders.append(self._TEMPLATE_WINDOWS)
		return builders
	
	def get_builders(self):
		builders = []
		if self._config.build_editor_linux:
			builders.append(util.BuilderConfig(name = self._EDITOR_LINUX, workernames = [self._config.worker_name], factory = linux.editor_64()))
		if self._config.build_editor_macos:
			builders.append(util.BuilderConfig(name = self._EDITOR_MACOS, workernames = [self._config.worker_macos_name], factory = macos.editor()))
		if self._config.build_editor_windows:
			builders.append(util.BuilderConfig(name = self._EDITOR_WINDOWS, workernames = [self._config.worker_name], factory = windows.editor_64()))

		builders.append(util.BuilderConfig(name = self._HEADLESS_LINUX, workernames = [self._config.worker_name], factory = linux.headless()))

		if self._config.build_template_android:
			builders.append(util.BuilderConfig(name = self._TEMPLATE_ANDROID, workernames = [self._config.worker_name], factory = android.templates()))
		if self._config.build_template_ios:
			builders.append(util.BuilderConfig(name = self._TEMPLATE_IOS, workernames = [self._config.worker_macos_name], factory = ios.template()))
		if self._config.build_template_linux:
			builders.append(util.BuilderConfig(name = self._TEMPLATE_LINUX, workernames = [self._config.worker_name], factory = linux.templates()))
		if self._config.build_template_macos:
			builders.append(util.BuilderConfig(name = self._TEMPLATE_MACOS, workernames = [self._config.worker_macos_name], factory = macos.template()))
		if self._config.build_template_web:
			builders.append(util.BuilderConfig(name = self._TEMPLATE_WEB, workernames = [self._config.worker_name], factory = web.templates()))
		if self._config.build_template_windows:
			builders.append(util.BuilderConfig(name = self._TEMPLATE_WINDOWS, workernames = [self._config.worker_name], factory = windows.templates()))

		if self._config.is_any_template_build:
			builders.append(util.BuilderConfig(name = self._ZIP_TEMPLATES, workernames = [self._config.worker_name], factory = zip.zip_templates()))
			if self._config.export_game:
				builders.append(util.BuilderConfig(name = self._EXPORT_GAME, workernames = [self._config.worker_name], factory = export.export()))
				if self._config.export_game_ios:
					builders.append(util.BuilderConfig(name = self._EXPORT_GAME_IOS, workernames = [self._config.worker_macos_name], factory = export.export_ios()))

		return builders


	_EDITOR_LINUX = 'linux-editor-64'
	_EDITOR_MACOS = 'macos-editor'
	_EDITOR_WINDOWS = 'win-editor-64'
	_HEADLESS_LINUX = 'linux-headless'
	_TEMPLATE_ANDROID = 'android-template'
	_TEMPLATE_IOS = 'ios-template'
	_TEMPLATE_MACOS = 'macos-template'
	_TEMPLATE_LINUX = 'linux-template'
	_TEMPLATE_WEB = 'web-template'
	_TEMPLATE_WINDOWS = 'win-template'
	_ZIP_TEMPLATES = 'zip-templates'
	_EXPORT_GAME = 'export'
	_EXPORT_GAME_IOS = 'export-ios'
