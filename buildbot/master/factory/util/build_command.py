from factory.util.config import Config


class BuildCommand:
	def __init__(self):
		config = Config()
		self.__command = [self.__SCONS]
		self.jobs(config.build_jobs_count)

	def __str__(self):
		return ' '.join(self.__command)

	def command(self):
		return self.__command

	def template(self):
		return self.tools(False)
	
	def tools(self, value):
		return self.__append(self.__TOOLS_YES) if value else self.__append(self.__TOOLS_NO)

	def android(self):
		return self.__append(self.__PLATFORM_ANDROID)

	def ios(self):
		return self.__append(self.__PLATFORM_IOS)

	def javascript(self):
		return self.__append(self.__PLATFORM_JAVASCRIPT)

	def linux(self):
		return self.__append(self.__PLATFORM_LINUX)

	def macos(self):
		return self.__append(self.__PLATFORM_MACOS)

	def server(self):
		return self.__append(self.__PLATFORM_SERVER)

	def windows(self):
		return self.__append(self.__PLATFORM_WINDOWS)

	def android_armv7(self):
		return self.__append(self.__ANDROID_ARCH_ARMV7)

	def android_arm64v8(self):
		return self.__append(self.__ANDROID_ARCH_ARM64V8)

	def android_x86(self):
		return self.__append(self.__ANDROID_ARCH_X86)

	def android_x86_64(self):
		return self.__append(self.__ANDROID_ARCH_X86_64)

	def ios_arm(self):
		return self.__append(self.__IOS_ARCH_ARM)

	def ios_arm64(self):
		return self.__append(self.__IOS_ARCH_ARM64)

	def ios_x86_64(self):
		return self.__append(self.__IOS_ARCH_X86_64).__append(self.__IOS_SIMULATOR)

	def macos_arm64(self):
		return self.__append(self.__MACOS_ARCH_ARM64)

	def macos_x86_64(self):
		return self.__append(self.__MACOS_ARCH_X86_64)

	def debug(self):
		return self.__append(self.__TARGET_DEBUG)

	def release(self):
		return self.__append(self.__TARGET_RELEASE)

	def release_debug(self):
		return self.__append(self.__TARGET_RELEASE_DEBUG)
	
	def no_debug_symbols(self):
		return self.__append(self.__DEBUG_SYMBOLS_NO)

	def bits_32(self):
		return self.__append(self.__BITS_32)

	def bits_64(self):
		return self.__append(self.__BITS_64)
	
	def jobs(self, n):
		return self.__append('-j' + str(n))

	def __append(self, param):
		self.__command.append(param)
		return self

	__SCONS = 'scons'
	__TOOLS_NO = 'tools=no'
	__TOOLS_YES = 'tools=yes'
	__PLATFORM_ANDROID = 'platform=android'
	__PLATFORM_IOS = 'platform=iphone'
	__PLATFORM_JAVASCRIPT = 'platform=javascript'
	__PLATFORM_LINUX = 'platform=x11'
	__PLATFORM_MACOS = 'platform=osx'
	__PLATFORM_SERVER = 'platform=server'
	__PLATFORM_WINDOWS = 'platform=windows'
	__ANDROID_ARCH_ARMV7 = 'android_arch=armv7'
	__ANDROID_ARCH_ARM64V8 = 'android_arch=arm64v8'
	__ANDROID_ARCH_X86 = 'android_arch=x86'
	__ANDROID_ARCH_X86_64 = 'android_arch=x86_64'
	__IOS_ARCH_ARM = 'arch=arm'
	__IOS_ARCH_ARM64 = 'arch=arm64'
	__IOS_ARCH_X86_64 = 'arch=x86_64'
	__IOS_SIMULATOR = 'ios_simulator=yes'
	__MACOS_ARCH_ARM64 = 'arch=arm64'
	__MACOS_ARCH_X86_64 = 'arch=x86_64'
	__TARGET_DEBUG = 'target=debug'
	__TARGET_RELEASE = 'target=release'
	__TARGET_RELEASE_DEBUG = 'target=release_debug'
	__DEBUG_SYMBOLS_NO = 'debug_symbols=no'
	__BITS_32 = 'bits=32'
	__BITS_64 = 'bits=64'
