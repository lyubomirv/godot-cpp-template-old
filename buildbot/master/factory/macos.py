import os

from buildbot.plugins import *

from factory.util.build_command import BuildCommand
from factory.util.util import get_compile_step
from factory.util.util import get_strip_step
from factory.util.util import get_lipo_step
from factory.util.util import create_factory_and_checkout
from factory.util.util import add_files_upload_steps

def get_build_command():
	return BuildCommand().macos()

def get_build_command_editor():
	return get_build_command().release_debug().no_debug_symbols()

def get_godot_dir():
	return 'godot'

def get_godot_output_dir():
	return os.path.normpath(os.path.join(get_godot_dir(), 'bin'))

def get_godot_output_workdir():
	return os.path.normpath(os.path.join('build', get_godot_output_dir()))

def get_godot_workdir():
	return os.path.normpath(os.path.join('build', get_godot_dir()))

def compile_and_strip(factory, build_command, filename):
	factory.addStep(get_compile_step(build_command, get_godot_workdir()))
	factory.addStep(get_strip_step(get_output_filename(filename)))

def get_output_filename(filename):
	return os.path.normpath(os.path.join(get_godot_output_dir(), filename))

def create_app(factory, app_template, executable_filenames, output_app_filename, output_zip_filename, renamed_filenames = []):
	factory.addStep(steps.RemoveDirectory(
		dir = os.path.normpath(os.path.join(get_godot_output_workdir(), output_app_filename))))
	factory.addStep(steps.ShellCommand(
		command = ['rm', '-f', output_zip_filename],
		workdir = get_godot_output_workdir(),
		description = 'deleting zip', descriptionDone = 'delete zip'))

	factory.addStep(steps.CopyDirectory(
		src = os.path.normpath(os.path.join(get_godot_workdir(), app_template)),
		dest = os.path.normpath(os.path.join(get_godot_output_workdir(), output_app_filename))))
	factory.addStep(steps.MakeDirectory(
		dir = os.path.normpath(os.path.join(get_godot_output_workdir(), output_app_filename, 'Contents/MacOS'))))

	for i, executable_filename in enumerate(executable_filenames):
		filename_in_bundle = renamed_filenames[i] if len(renamed_filenames) > i else executable_filename
		filename_in_bundle_path = os.path.normpath(os.path.join(output_app_filename, 'Contents/MacOS', filename_in_bundle))
		factory.addStep(steps.ShellCommand(
			command = ['cp', executable_filename, filename_in_bundle_path],
			workdir = get_godot_output_workdir(),
			description = 'copying binary', descriptionDone = 'copy binary'))
		factory.addStep(steps.ShellCommand(
			command = ['chmod', '+x', filename_in_bundle_path],
			workdir = get_godot_output_workdir(),
			description = 'making executable', descriptionDone = 'make executable'))
	
	factory.addStep(steps.ShellCommand(
		command = ['zip', '-r', output_zip_filename, output_app_filename],
		workdir = get_godot_output_workdir(),
		description = 'zipping app', descriptionDone = 'zip app'))

###############################################################################
### Editor
def editor():
	factory = create_factory_and_checkout()

	filename_x86_64 = 'godot.osx.opt.tools.x86_64'
	filename_arm64 = 'godot.osx.opt.tools.arm64'

	build_command_x86_64 = get_build_command_editor().macos_x86_64().command()
	compile_and_strip(factory, build_command_x86_64, filename_x86_64)

	build_command_arm64 = get_build_command_editor().macos_arm64().command()
	compile_and_strip(factory, build_command_arm64, filename_arm64)

	filenames = [filename_x86_64, filename_arm64]
	output_filename = 'godot.osx.opt.tools.universal'
	factory.addStep(get_lipo_step(filenames, output_filename))

	create_app(factory, 'misc/dist/osx_tools.app', [output_filename], 'godot.osx.app', 'godot.osx.zip')
	add_files_upload_steps(factory, ['godot.osx.zip'], 'editor')

	return factory

###############################################################################
### Template

def get_build_command_template(is_release):
	build_command = get_build_command().template()
	build_command = build_command.release().no_debug_symbols() if is_release else build_command.debug()
	return build_command

def get_template_filenames(is_release):
	filename_base = 'godot.osx.'
	filename_base += 'opt' if is_release else 'debug'
	filename_x86_64 = filename_base + '.x86_64'
	filename_arm64 = filename_base + '.arm64'
	return filename_x86_64, filename_arm64

def template_compile(factory, is_release):
	filename_x86_64, filename_arm64 = get_template_filenames(is_release)

	build_command = get_build_command_template(is_release).macos_x86_64()
	compile_and_strip(factory, build_command.command(), filename_x86_64)

	build_command = get_build_command_template(is_release).macos_arm64()
	compile_and_strip(factory, build_command.command(), filename_arm64)

	filenames = [filename_x86_64, filename_arm64]
	output_filename = 'godot.osx.'
	output_filename += 'opt' if is_release else 'debug'
	output_filename += '.universal'
	factory.addStep(get_lipo_step(filenames, output_filename))

def template():
	factory = create_factory_and_checkout()

	template_compile(factory, False)
	template_compile(factory, True)

	# NOTE: The executables are renamed with the .64 suffix even though
	# they are universal binaries. This is because the godot editor expects
	# these filenames in the template by default.
	create_app(
		factory,
		'misc/dist/osx_template.app',
		['godot.osx.debug.universal', 'godot.osx.opt.universal'],
		'osx_template.app', 'osx.zip',
		['godot_osx_debug.64', 'godot_osx_release.64'])
	add_files_upload_steps(factory, ['osx.zip'], 'templates')

	return factory

