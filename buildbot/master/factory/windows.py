from buildbot.plugins import *

from factory.util.build_command import BuildCommand
from factory.util.config import Config
from factory.util.util import add_desktop_template_steps
from factory.util.util import create_editor_factory
from factory.util.util import create_factory_and_checkout

def get_build_command():
	return BuildCommand().windows()

def editor_64():
	return create_editor_factory('win-editor-64', Config().docker_image_windows, get_build_command(), 'godot.windows.opt.tools.64.exe')

def templates():
	factory = create_factory_and_checkout()

	builder = 'win-template'
	docker_image = Config().docker_image_windows
	filename_base = 'godot.windows'
	filename_ext = 'exe'
	filename_final_base = 'windows_'
	add_desktop_template_steps(factory, builder, docker_image, get_build_command(), 64, False, filename_base, filename_ext, filename_final_base + '64_debug.exe')
	add_desktop_template_steps(factory, builder, docker_image, get_build_command(), 64, True, filename_base, filename_ext, filename_final_base + '64_release.exe')
	add_desktop_template_steps(factory, builder, docker_image, get_build_command(), 32, False, filename_base, filename_ext, filename_final_base + '32_debug.exe')
	add_desktop_template_steps(factory, builder, docker_image, get_build_command(), 32, True, filename_base, filename_ext, filename_final_base + '32_release.exe')

	return factory
