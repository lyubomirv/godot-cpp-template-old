import os

from buildbot.plugins import *

from factory.util.build_command import BuildCommand
from factory.util.util import add_files_upload_steps
from factory.util.util import create_factory_and_checkout
from factory.util.util import get_compile_step
from factory.util.util import get_lipo_step
from factory.util.util import get_strip_step

def get_build_command():
	return BuildCommand().ios().template()

def get_godot_dir():
	return 'godot'

def get_godot_output_dir():
	return os.path.normpath(os.path.join(get_godot_dir(), 'bin'))

def get_godot_output_workdir():
	return os.path.normpath(os.path.join('build', get_godot_output_dir()))

def get_godot_workdir():
	return os.path.normpath(os.path.join('build', get_godot_dir()))

def add_template_steps(factory, build_command, is_release, filename_suffix):
	build_command = build_command.release() if is_release else build_command.debug()
	build_command = build_command.no_debug_symbols()
	factory.addStep(get_compile_step(build_command.command(), get_godot_workdir()))

	iphone_suffix = '.iphone'
	filename = 'libgodot' + iphone_suffix + filename_suffix
	factory.addStep(get_strip_step(
		os.path.normpath(os.path.join(get_godot_output_dir(), filename))))

def get_lipo_step_for_file(filename_base, is_release):
	source_filename_base = filename_base
	source_filename_base += '.'
	source_filename_base += 'opt' if is_release else 'debug'
	source_filename_base += '.'
	filenames = [
		source_filename_base + 'arm.a',
		source_filename_base + 'arm64.a',
		source_filename_base + 'x86_64.a']

	output_filename = filename_base
	output_filename += '.'
	output_filename += 'release' if is_release else 'debug'
	output_filename += '.fat.a'

	return get_lipo_step(filenames, output_filename)

def create_template(factory, template_dir, lib_filenames, output_dir):
	output_zip_filename = output_dir + '.zip'
	factory.addStep(steps.RemoveDirectory(
		dir = os.path.normpath(os.path.join(get_godot_output_workdir(), output_dir))))
	factory.addStep(steps.ShellCommand(
		command = ['rm', '-f', output_zip_filename],
		workdir = get_godot_output_workdir(),
		description = 'deleting zip', descriptionDone = 'delete zip'))

	factory.addStep(steps.CopyDirectory(
		src = os.path.normpath(os.path.join(get_godot_workdir(), template_dir)),
		dest = os.path.normpath(os.path.join(get_godot_output_workdir(), output_dir))))

	for lib_filename in lib_filenames:
		factory.addStep(steps.ShellCommand(
			command = ['cp', lib_filename, output_dir],
			workdir = get_godot_output_workdir(),
			description = 'copying ' + lib_filename, descriptionDone = 'copy ' + lib_filename))
	
	factory.addStep(steps.ShellCommand(
		command = ['zip', '-r', output_zip_filename, output_dir],
		workdir = get_godot_output_workdir(),
		description = 'zipping template', descriptionDone = 'zip template'))

def template():
	factory = create_factory_and_checkout()

	add_template_steps(factory, get_build_command().ios_arm(), False, '.debug.arm.a')
	add_template_steps(factory, get_build_command().ios_arm(), True, '.opt.arm.a')
	add_template_steps(factory, get_build_command().ios_arm64(), False, '.debug.arm64.a')
	add_template_steps(factory, get_build_command().ios_arm64(), True, '.opt.arm64.a')
	add_template_steps(factory, get_build_command().ios_x86_64(), False, '.debug.x86_64.a')
	add_template_steps(factory, get_build_command().ios_x86_64(), True, '.opt.x86_64.a')

	iphone_suffix = '.iphone'
	filename_base = 'libgodot' + iphone_suffix
	factory.addStep(get_lipo_step_for_file(filename_base, False))
	factory.addStep(get_lipo_step_for_file(filename_base, True))

	lib_filenames = []
	lib_filenames.append(filename_base + '.debug.fat.a')
	lib_filenames.append(filename_base + '.release.fat.a')
	create_template(factory, 'misc/dist/ios_xcode', lib_filenames, 'iphone')

	add_files_upload_steps(factory, ['iphone.zip'], 'templates')

	return factory
