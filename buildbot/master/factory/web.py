import os

from buildbot.plugins import *

from factory.util.build_command import BuildCommand
from factory.util.config import Config
from factory.util.util import add_file_upload_steps
from factory.util.util import create_factory_and_checkout
from factory.util.util import get_compile_step_docker
from factory.util.util import get_godot_output_dir

def get_build_command():
	return BuildCommand().javascript().tools(False).no_debug_symbols()

def build_template(factory, is_release):
	builder = 'web-template'
	docker_image = Config().docker_image_web
	build_command = get_build_command()
	build_command = build_command.release() if is_release else build_command.release_debug()
	factory.addStep(
		get_compile_step_docker(
			builder,
			docker_image,
			build_command.command()))
	
	filename = 'godot.javascript.opt.zip' if is_release else 'godot.javascript.opt.debug.zip'
	original_filename = os.path.normpath(os.path.join(get_godot_output_dir(), filename))
	final_filename = 'webassembly_release.zip' if is_release else 'webassembly_debug.zip'
	factory.addStep(
		steps.ShellCommand(
			command = ['cp', original_filename, final_filename],
			description = 'renaming template' ,descriptionDone = 'rename template'))
	add_file_upload_steps(factory, final_filename, '', 'templates')

def templates():
	factory = create_factory_and_checkout()

	build_template(factory, False)
	build_template(factory, True)

	return factory
