import os

from buildbot.plugins import *

from factory.util.util import add_file_upload_steps
from factory.util.util import create_factory_and_checkout
from factory.util.util import create_factory_and_get_revision
from factory.util.util import get_chown_step
from factory.util.util import get_file_download_step

GODOT_HEADLESS = 'godot_server.x11.opt.tools.64'
HOME_DIR = '/home/buildbot/'
TEMPLATES_DIR = HOME_DIR + '.local/share/godot/templates/3.4.stable/'
CONFIG_DIR = HOME_DIR + '.config/godot/'
OUTPUT_DIR = 'output'

def get_project_workdir():
	return os.path.normpath(os.path.join('build', 'project'))

def get_output_workdir():
	return os.path.normpath(os.path.join('build', OUTPUT_DIR))

def get_output_dir(template):
	return os.path.normpath(os.path.join(OUTPUT_DIR, template))

def get_output_dir_with_build(template):
	return os.path.normpath(os.path.join('build', get_output_dir(template)))

def get_godot_headless_path_from_project():
	return os.path.normpath(os.path.join('..', GODOT_HEADLESS))

def get_output_dir_from_project(template):
	return os.path.normpath(os.path.join('..', get_output_dir(template)))

def get_output_filename_from_project(directory, filename):
	return os.path.normpath(os.path.join(get_output_dir_from_project(directory), filename))

def get_download_headless_step():
	return get_file_download_step(GODOT_HEADLESS, GODOT_HEADLESS, 'headless', True)

def download_templates(factory):
	templates_zip = 'templates.zip'
	factory.addStep(get_file_download_step('templates.zip', TEMPLATES_DIR + '/' + 'templates.zip'))
	factory.addStep(steps.ShellCommand(
		command = ['unzip', '-o', '-j', templates_zip],
		workdir = TEMPLATES_DIR,
		haltOnFailure = True,
		description = 'unzipping templates', descriptionDone = 'unzip templates'))

def download_intermediate(factory, filename_zip):
	factory.addStep(get_file_download_step(filename_zip, filename_zip, 'intermediate'))
	factory.addStep(steps.ShellCommand(
		command = ['unzip', '-o', filename_zip],
		haltOnFailure = True,
		description = 'unzipping intermediate', descriptionDone = 'unzip intermediate'))

def export_template(factory, template, export_filename = '', zip = True, upload = True, release = True, env = {}):
	factory.addStep(steps.MakeDirectory(dir = get_output_dir_with_build(template)))
	factory.addStep(
		steps.ShellCommand(
			command = [
				get_godot_headless_path_from_project(),
				'--export' if release else '--export-debug',
				template,
				get_output_filename_from_project(template, export_filename if export_filename else template),
				'--verbose'],
			workdir = get_project_workdir(),
			env = env,
			haltOnFailure = True,
			description = 'building ' + template, descriptionDone = 'build ' + template))

	game_zip = template + '.zip'
	if zip:
		factory.addStep(
			steps.ShellCommand(
				command = ['zip', '-r', game_zip, template],
				workdir = get_output_workdir(),
				haltOnFailure = True,
				description = 'zipping ' + template, descriptionDone = 'zip ' + template))

	if upload:
		add_file_upload_steps(factory, game_zip, OUTPUT_DIR, 'game')

def export_template_macos(factory):
	template = 'macos'
	output_filename = template + '.zip'
	export_template(factory, template, output_filename, False, False)
	
	add_file_upload_steps(factory, output_filename, get_output_dir(template), 'game')

def get_replace_config_value_step(key, value_old, value_new):
	sed_expr = 's|' + key + ' = "' + value_old + '"|' + key + ' = "' + value_new + '"|'
	return steps.ShellCommand(
		command = [
			'sed', '-i',
			sed_expr,
			'editor_settings-3.tres'],
		workdir = CONFIG_DIR,
		haltOnFailure = True,
		description = 'setting config value for ' + key, descriptionDone = 'set config value for ' + key)

# NOTE: This function expects that the export process has been run and therefore
# the editor settings file exists at: ~/.config/godot/editor_settings-3.tres
def export_template_android(factory):
	factory.addStep(get_replace_config_value_step('export/android/adb', '', '/tools/android/adb.sh'))
	factory.addStep(get_replace_config_value_step('export/android/jarsigner', '', '/tools/android/jarsigner.sh'))
	factory.addStep(get_replace_config_value_step('export/android/android_sdk_path', '', '/tools/android/sdk'))

	factory.addStep(get_chown_step('/tmp/cache', 1000, 1000))

	# These actually combine two architectures each:
	#  - arm combines armv7 and arm64
	#  - x86 combines x86 and x86_64
	templates = ['android.arm', 'android.x86']
	for template in templates:
		output_filename_debug = template + '.debug.apk'
		output_filename_release = template + '.release.apk'
		env = { 'XDG_CACHE_HOME': '/tmp/cache' }

		export_template(factory, template, output_filename_debug, False, False, False, env)
		export_template(factory, template, output_filename_release, False, False, True, env)
		add_file_upload_steps(factory, output_filename_debug, get_output_dir(template), 'game')
		add_file_upload_steps(factory, output_filename_release, get_output_dir(template), 'game')

# NOTE: Exporting for ios is a two-step process - first is the regular export by
# the godot engine (like the other platforms) which generates a XCode project and
# then that project has to be built by XCode on a MacOS
# This being executed on Linux, only the first step is done, and then the resulting
# project will be transfered (via the master) to the MacOS builder, which will call XCode
# The final build is done by the MacOS builder which calls the function export_ios() below
def export_template_ios(factory):
	export_template(factory, 'ios', 'ios.ipa', True, False)
	add_file_upload_steps(factory, 'ios.zip', OUTPUT_DIR, 'intermediate')

def export():
	factory = create_factory_and_checkout(False)
	factory.addStep(get_download_headless_step())
	download_templates(factory)

	export_template(factory, 'linux.x86_64')
	export_template(factory, 'linux.x86')
	export_template(factory, 'windows.x86_64', 'windows.x86_64.exe')
	export_template(factory, 'windows.x86', 'windows.x86.exe')
	export_template_macos(factory)
	export_template_android(factory)
	export_template_ios(factory)
	export_template(factory, 'web', 'web.html')

	return factory

def export_ios():
	factory = create_factory_and_get_revision(True)
	download_intermediate(factory, 'ios.zip')
	factory.addStep(steps.ShellCommand(
		command = [
			'xcodebuild',
			'-project', 'ios.xcodeproj',
			'-target', 'ios',
			'-configuration', 'Debug'],
		workdir = 'build/ios',
		haltOnFailure = True,
		description = 'building ios', descriptionDone = 'build ios'))
	return factory

